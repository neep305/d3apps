d3.tsv("../mydata.tsv", function(error, data){
  console.log(data);
  var dataSet = [];
  for (var i=0; i < data.length; i++) {
    dataSet.push(data[i].item1);
  }
  //Draw a graph
  d3.select("#myGraph")
  .selectAll("rect")
  .data(dataSet)
  .enter()
  .append("rect")
  .attr("class", "bar")
  .attr("width", function(d,i){
    console.log(d);
    return d;
  })
  .attr("height", 20)
  .attr("x", 0)
  .attr("y", function(d,i){
    return i*25;
  })
})
