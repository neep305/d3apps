d3.csv("../../static/Sacramentorealestatetransactions.csv", function(error,data){
  console.log(data);
  var dataSet = [];

  for (var i=0; i<data.length; i++) {
    dataSet.push(data[i].price);
  }

  d3.select("#myGraph")
  .selectAll("rect")
  .data(dataSet)
  .enter()
  .append("rect")
  .attr("class","bar")
})
