d3.csv("../../static/mydata.csv", function(error,data){
  console.log(data);
  var dataSet = [];
  for(var i=0; i<data.length; i++) {
    dataSet.push(data[i].item1);
  }

  //Draw a graph
  d3.select("#myGraph")
  .selectAll("rect")
  .data(dataSet)
  .enter()
  .append("rect")
  .call(function(element){
    element.each(function(d,i){
      console.log(i+" = " +d);
    })
  })
  .attr("class","bar")
  .attr("width", function(d,i){
    return d;
  })
  .attr("height", 20)
  .attr("x",0)
  .attr("y", function(d,i){
    return (i+1) * 25
  })
})
