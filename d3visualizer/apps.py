from django.apps import AppConfig


class D3VisualizerConfig(AppConfig):
    name = 'd3visualizer'
